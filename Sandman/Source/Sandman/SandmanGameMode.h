// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "SandmanGameMode.generated.h"

UCLASS(minimalapi)
class ASandmanGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ASandmanGameMode(const FObjectInitializer& ObjectInitializer);
};



